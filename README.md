# YouTube-Stalker
## A YouTube channel tracker that rips music of youtube and other sites
### Summary
YouTube-Stalker Is a piece of server software that utilizes youtube-dl and SQLite to keep track of YouTube Channels.
It will use youtube-dl as its main backend and mainly support YouTube and Soundloud.

#### Features
YouTube-Stalker will contain the following features:
- Subscription via channel URL.
- Extracting and converting the audio from the video to multiple formats.
- A configuration file with a special directory for channel specific configurations.
- A SQLite database to keep track of what has been downloaded and what has been converted.
  - This feature makes sure that the program does not download the same video twice.
  - A tool to manipulate and interact with the database.
    - This is useful for re-downloading certain videos or to reconvert them.
      - Will also be included in the tool itself.
- Extraction of thumbnails from the video itself to be set as album art.
- Tho this is more useful for soundcloud.
- Minimal effort tagging of the audio files.
